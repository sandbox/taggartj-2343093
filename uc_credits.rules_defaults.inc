<?php
/*http://jamestombs.co.uk/2012-09-19/drupal-7-defining-rules-within-module-using-php*/
function uc_credits_default_rules_configuration() {
  $configs = array();
   $rule = '{ "uc_credits_checkout_addcredits" : {
    "LABEL" : "Uc Credits ",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "REQUIRES" : [ "uc_payment", "php", "rules" ],
    "ON" : { "uc_payment_entered" : [] },
    "IF" : [
      { "uc_payment_condition_order_balance" : {
          "order" : [ "order" ],
          "balance_comparison" : "less_equal",
          "include_authorizations" : 1
        }
      }
    ],
    "DO" : [ { "php_eval" : { "code" : "uc_credits_credits_update($order);" } } ]
  }
}';
   $configs['uc_credits_checkout_addcredits'] = rules_import($rule);
   
  return $configs;
}